# Lebenslauf·_Curriculum vitae_
## Paul John King

## Persönliche Angaben·_Personal Details_

**Staatsangehörigkeit·_Citizenship_**    
Deutsch & Britisch·_German & British_    
**Sprachen·_Languages_**    
Deutsch (CEFR B1) & Englisch (Heimatsprache)    
_German (CEFR B1) & English (native tongue)_    
**E-Mail·_Email_**    
[paul_john_king@web.de](mailto:paul_john_king@web.de)    
**Links·_Links_**    
[GitLab Repos](https://gitlab.com/paul_john_king)    
[GitLab Pages](https://paul_john_king.gitlab.io)    
[LinkedIn](https://www.linkedin.com/in/paul-john-king-6a9696175)    
[XING](https://www.xing.com/profile/PaulJohn_King/)    

## Arbeit·_Employment_

**IT Administrator (87.5% Teilzeit)**    
**_IT Administrator (87.5% part-time)_**    
[Agile Robots AG](https://www.agile-robots.com/),    
Staffelseestr. 8,    
D-81477 München,    
Deutschland.    
2021-08–    

**IT-Systemingenieur Software-definierte Infrastruktur (87.5% Teilzeit, 2 Monate befristeter Vertrag)**    
**_IT System Engineer Software Defined Infrastructure (87.5% part-time, 2 month fixed-term contract)_**    
[open*i GmbH](https://www.openinfrastructure.de/),    
Talstr. 41,    
D-70188 Stuttgart,    
Deutschland.    
2021-06–2021-07    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/openi_2.pdf).    
>>>
_A customer required us to split, with limited down-time, a
single Icinga instance into two independent instances.  I wrote scripts (in
Bourne shell) to extract databases for the independent instances from the SQL
and InfluxDB databases of the single instance with no overlap of sensitive data
and no loss of relevant history._
>>>

**DevOps Oberingenieur (87.5% Teilzeit)**    
**_Senior DevOps Engineer (87.5% part-time)_**    
[XALT Business Consulting GmbH](https://www.xalt.de/),    
Tomannweg 3,    
D-81673 München,    
Deutschland.    
2021-03–2021-05    
>>>
_I was sacked – the only time in my life – but was extremely
pleased to leave._
>>>

**IT-Systemingenieur Software-definierte Infrastruktur (80% Teilzeit)**    
**_IT System Engineer Software Defined Infrastructure (80% part-time)_**    
[open*i GmbH](https://www.openinfrastructure.de/),    
Talstr. 41,    
D-70188 Stuttgart,    
Deutschland.    
2019-02–2021-02    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/openi_1.pdf).    
>>>
_I wrote tools that help the company administrators manage the
infrastructure of the company customers.  The tools include a Python script
that warns if an Icinga2 server fails to send a notification; a Go program that
updates the configuration of a Prometheus server if entries with specified
properties in a Consul store change; a Bourne shell script that generates an
availability report from an Icinga2 database; and a Python package that reads
XML documents from an HTTP(S), RabbitMQ or IBM MQ server, converts the
documents to JSON data, and writes the data as active or passive checks to an
Icinga2
REST server._
>>>

**Linux-Systemadministrator (75% Teilzeit)**    
**_Linux System Administrator (75% part-time)_**    
[levigo solutions gmbh](https://solutions.levigo.de/),    
Bebelsbergstr. 31,    
D-71088 Holzgerlingen,    
Deutschland.    
2014-08–2018-12.    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/levigo-solutions.pdf).    
>>>
_I administered the applications (Confluence, Jira, Nexus
and Stash) that the company developers use, and the Linux machines that host
them.  I built a continuous-delivery pipeline (with Jenkins, Maven, Puppet and
tools I wrote in Bourne shell, Python and Ruby) in order to write Jira plug-ins
(in Java).  I built the compute component of the IaaS layer of a private cloud
(with Alpine Linux, Ansible, KVM/QEMU, `libvirt`, Packer, PXE and
RancherOS)._
>>>

**Briefzusteller (50% Teilzeit)**    
**_Mail carrier (50% part-time)_**    
sMAIL,    
GEA Post-Service GmbH,    
Auchertstr. 4,    
D-72770 Reutlingen,    
Deutschland.    
2014-02–2014-07.    

**Systemadministrator**    
**_System Administrator_**    
Universitätsklinikum Tübingen,    
Geschäftsbereich Informationstechnologie,    
Geissweg 11,    
D-72076 Tübingen,    
Deutschland.    
2013-03–2013-09.    
>>>
_An Oracle RDBMS administrator and I migrated over 20
applications from the Enterprise edition to the Standard edition of the Oracle
RDBMS within a 7 month deadline in order to avoid expensive licensing
fees._
>>>

**Linux-Systemadministrator (60% Teilzeit)**    
**_Linux System Administrator (60% part-time)_**    
NETlution GmbH,    
Julius-Hatry-Str. 1,    
D-68163 Mannheim,    
Deutschland.    
2012-02–2013-02.    
>>>
_I worked at the same broadcaster I worked at with Cancom,
and a bank.  At the broadcaster, I administered about 250 Linux machines,
provided first-level VMware support, configured Linux machines as Oracle RDBMS
servers, and rewrote the system that transfers time-critical messages (such as
motorway hazards) to the broadcaster.  At the bank, I administered about a
dozen Linux machines, wrote Nagios modules and UC4 scripts, and wrote a script
(in `bash`) to inventorise the Linux stock._
>>>

**Systemadministrator (60% Teilzeit)**    
**_System Administrator (60% part-time)_**    
CANCOM IT Solutions GmbH,    
Max-Eyth-Str. 21,    
D-72622 Nürtingen,    
Deutschland.    
2009-07–2012-01.    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/Cancom.pdf).    
>>>
_I worked at a regional radio and television broadcaster.  I
commissioned and decommissioned physical and virtual servers for campuses in
Stuttgart, Baden-Baden and Mainz.  I reduced the mean commission time of a
server from six weeks to five days by writing a web application (in Java, JDBC
and XSLT) that extracts and presents all of the information concerning server
commissions and decommissions from the broadcaster's work-flow and inventory
databases.  I also helped transfer over 150 physical servers to a new server
room on the Stuttgart campus while keeping all “hot” services online._
>>>

**Java- & JavaScript-Programmierer (16 Monate befristeter Vertrag)**    
**_Java & JavaScript Programmer (16 month fixed-term contract)_**    
The Prometheus Project,    
Universitätsklinikum Tübingen,    
Neuroradiologie,    
Hoppe-Seyler-Str. 3,    
D-72076 Tübingen,    
Deutschland.    
2002-09–2003-12.    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/Prometheus.pdf).    
>>>
_As part of a fixed-term project to build a medical teaching and
examination tool, I wrote a client/server library (in JavaScript and Java) to
create a "desktop in a browser" that includes a task bar and icon-activated
windows containing text and media that can be restacked, moved, resized,
hidden, restored and closed._
>>>

**Linux-Systemadministrator & Open-Source-Programmierer (50% Teilzeit)**    
**_Linux System Administrator & Open Source Programmer (50% part-time)_**    
„pc-online‟ Computer Handels GmbH,    
Lilli-Zapf-Str. 2,    
D-72072 Tübingen,    
Deutschland.    
2000-01–2002-08.    
[Arbeitszeugnis·_Reference_](https://paul_john_king.gitlab.io/references/pc-online.pdf).    
>>>
_I provided Linux administration services - primarily Amanda
back-up, `iptables` packet filtering, and Samba file and print
sharing - to several small to medium businesses.  I also wrote software for a
few customers, including an application (first in `tcsh`, later in
Perl) that uses hard links to generate space-efficient yet readily-accessible
time-stamped back-ups of directory trees, and a server-side web application
(first in Perl, then in PHP) that shows a user the nearest stockist of the
customer's product on a zoomable map._
>>>

**Wissenschaftlicher Koordinator**    
**_Scientific Coordinator_**    
The CLaRK (Computational Linguistics and Represented Knowledge) Programme,    
Seminar für Sprachwissenschaft,    
Eberhard-Karls-Universität,    
Wilhelmstr. 113,    
D-72074 Tübingen,    
Deutschland.    
1998-10–1999-12.    

**Forschungsstipendiat**    
**_Research Fellow_**    
Graduiertenkolleg Integriertes Linguistik-Studium,    
Seminar für Sprachwissenschaft,    
Eberhard-Karls-Universität,    
Wilhelmstr. 113,    
D-72074 Tübingen,    
Deutschland.    
1997-06–1998-09.    

**Dozent**    
**_Lecturer_**    
Computerlinguistik Abteilung,    
Seminar für Sprachwissenschaft,    
Eberhard-Karls-Universität,    
Wilhelmstr. 113,    
D-72074 Tübingen,    
Deutschland.    
1995-01–1996-09.    

**Wissenschaftlicher Mitarbeiter**    
**_Scientific Assistant_**    
Teilprojekt B4 "Constraints on Grammar for Efficient Generation",    
Sonderforschungsbereich 340,    
Seminar für Sprachwissenschaft,    
Eberhard-Karls-Universität,    
Wilhelmstr. 113,    
D-72074 Tübingen,    
Deutschland.    
1992-01–1994-12.    

**Promovierter Stipendiat**    
**_Postdoctoral Fellow_**    
Center for the Study of Language and Information,    
Ventura Hall,    
Stanford University,    
Stanford,    
California 94305,    
USA.    
1990-01–1991-12.    

**Gärtner (freiberuflich)**    
**_Gardener (self-employed)_**    
England.    
1982-05–1983-09.    

**Laborant**    
**_Laboratory Assistant_**    
Process Instrumentation Evaluation Dept.,    
SIRA Institute Ltd.,    
South Hill,    
Chislehurst, Kent,    
England.    
1978-09–1982-04.    

## Studium & Fortbildung·_Studies & Further Education_

**NWLI3103, SUSE Linux Enterprise Server 12 Administration**    
the campus GmbH,    
Englschalkinger Str. 12,    
D-81925 München,    
Deutschland.    
2010-12.    

**NWLI3101, SUSE Linux Enterprise Server 12 Fundamentals**    
the campus GmbH,    
Englschalkinger Str. 12,    
D-81925 München,    
Deutschland.    
2010-11.    

**Doktor der Philosophie, mathematische Logik**    
**_Doctor of Philosophy, mathematical logic_**    
Department of Mathematics,    
University of Manchester,    
Oxford Road,    
Manchester,    
England.    
1986-10–1989-10.    

**Bakkalaureus der Wissenschaften, Mathematik**    
**_Bachelor of Science, mathematics_**    
Department of Mathematics,    
Queen Mary College,    
University of London,    
Mile End Road,    
London,    
England.    
1983-10–1986-07.    

## Befähigungsnachweise·_Qualifications_

**Doktor der Philosophie, mathematische Logik**    
**_Doctor of Philosophy, mathematical logic_**    
1990.    
[Abschlusszeugnis·_Diploma_](https://paul_john_king.gitlab.io/diplomas/Doctorate.pdf).    

**Bakkalaureus der Wissenschaften, Mathematik**    
**_Bachelor of Science, mathematics_**    
1986.    
[Abschlusszeugnis·_Diploma_](https://paul_john_king.gitlab.io/diplomas/Baccalaureate.pdf).    

**Linux Professional Institute Certification, level 1**    
2010-12.    
[Bescheinigung·_Certificate_](https://paul_john_king.gitlab.io/certificates/LPIC-1.pdf).    

**Novell Certified Linux Administrator, SUSE Linux Enterprise 12**    
2010-12.    
[Bescheinigung·_Certificate_](https://paul_john_king.gitlab.io/certificates/SCLP_SLES_12.pdf).    

## IT-Fähigkeiten·_IT Competences_

**Beschriftung·_Legend_**    
◆ Hauptanwendung in den letzten 2 Jahren·_Major application in the last 2 years_    
◈ Nebenanwendung in den letzten 2 Jahren oder Hauptanwendung in den letzten 10 Jahren·_Minor application in the last 2 years or major application in the last 10 years_    
◇ Nebenanwendung in den letzten 10 Jahren·_Minor application in the last 10 years_    

**Methodik·_Methodologies_**    
◆ Continuous Delivery    
◆ DevOps    
◆ Functional Programming    
◈ Immutable Infrastructure    
◆ Infrastructure as Code    
◆ Object-Oriented Programming    

**Programmiersprache·_Programming Languages_**    
◈ AWK    
◆ Bourne shell (`ash`, `busybox` & `dash`)    
◆ Bourne-again shell (`bash`)    
◈ C    
◆ Go    
◇ Groovy (für·_for_ Jenkins)    
◈ Java    
◈ Jinja2 (für·_for_ Ansible)    
◇ Perl    
◆ Python    
◈ Ruby    
◈ sed    
◆ SQL    
◇ UC4    

**Auszeichnungssprache·_Mark-up Languages_**    
◈ HTML & CSS    
◆ Markdown (GitHub, GitLab & Stash)    
◆ XML, XSD & XSLT    

**Versionsverwaltungswerkzeuge·_Version Control Tools_**    
◆ Git    
◈ Subversion    

**Erstellungsprozesswerkzeuge·_Build Automation Tools_**    
◇ Apache Ant    
◇ debuild    
◆ Docker    
◈ Jenkins    
◈ Make    
◈ Maven    
◈ Packer    
◆ rpmbuild    

**Linux-Distributionen & -Umgebungen·_Linux Distributions & Environments_**    
◆ Alpine Linux    
◆ BusyBox    
◇ CentOS    
◈ Debian    
◇ NixOS    
◈ RancherOS    
◈ SUSE Linux Enterprise Server    
◆ Ubuntu    

**Virtualisierungs-Plattformen·_Virtualisation Platforms_**    
◆ Docker    
◈ KVM/QEMU    
◈ libvirt    
◈ Vagrant    
◇ VMware    

**Konfigurationsmanagementsysteme·_Configuration Management Systems_**    
◆ Ansible    
◈ Puppet    

**relationales Datenbankenmanagementsysteme·_Relational Database Management Systems_**    
◈ Microsoft SQL Server    
◆ MySQL/MariaDB    
◈ Oracle Database Server    
◇ PostgreSQL    

**Dienste·_Services_**    
◈ DHCP    
◈ DNS    
◈ FTP    
◆ Icinga    
◆ Nagios    
◈ PXE & TFTP    
◈ SSH & SFTP    

**Anwendungen·_Applications_**    
◈ Apache HTTPD    
◈ Apache Tomcat    
◈ Atlassian Confluence    
◈ Atlassian Jira    
◈ Atlassian Stash    
◈ Sonatype Nexus    

## Begutachtete Veröffentlichungen·_Refereed Publications_

**Paul John King, 1989**    
_A Logical Formalism for Head-driven Phrase Structure Grammar_.    
Doktorarbeit·_Doctoral thesis_.    

**Dale Gerdemann & Paul John King, 1993**    
"Typed Feature Structures for Expressing and Computationally Implementing Feature Cooccurence Restrictions".    
_4. Fachtagung der Sektion Computerlinguistik der Deutschen Gessellschaft für Sprachwissenschaft_, 33–39.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Feature_Coocurrence_Restrictions.pdf).    

**Dale Gerdemann & Paul John King, 1994**    
"The Correct and Efficient Implementation of Appropriateness Specifications for Typed Feature Structures".    
_The Proceedings of COLING 94_, 956–960.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Appropriateness_Specifications.pdf).    

**Paul John King, 1994**    
"Typed Feature Structures as Descriptions".    
_The Proceedings of COLING 94_, 1250–1254.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Feature_Structures_as_Descriptions.pdf).    

**Paul John King, 1994**    
"Reconciling Austinian and Russellian Accounts of the Liar Paradox".    
_The Journal of Philosophical Logic_, 23.5, 451–494.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Accounts_of_the_Liar_Paradox.pdf).    

**Paul John King & Kiril Ivanov Simov, 1998**    
"The Automatic Deduction of Classificatory Systems from Linguistic Theories".    
_Grammars_, 1.2, 103–153.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Deduction_of_Classificatory_Systems.pdf).    

**Paul John King, 1999**    
"Towards Truth in Head-Driven Phrase Structure Grammar".    
Valia Kordoni (ed.), _Tübingen Studies in Head-Driven Phrase Structure Grammar_, 301-352.    
[Artikel·_Paper_](https://paul_john_king.gitlab.io/papers/Towards_Truth_in_HPSG.pdf).    

**Paul John King, Kiril Ivanov Simov & Bjørn Aldag, 1999**    
"The Complexity of Modelability in Finite and Computable Signatures of a Constraint Logic for Head-driven Phrase Structure Grammar".    
_The Journal of Logic, Language and Information_, 8.1, 83–110.    
[Artikel·_Article_](https://paul_john_king.gitlab.io/papers/Complexity_of_Modelability.pdf).    

