#!/bin/sh

set -e ;
set -u ;

_main() {
	local _BUILD_IMAGE="registry.gitlab.com/paul_john_king/docker.documentation_build:0.4.4_121bc47" ;
	local _BUILD_DIR="/build" ;

	docker run \
		--interactive \
		--tty \
		--rm \
		--user "$(id -u):$(id -g)" \
		--volume "${PWD}:${_BUILD_DIR}" \
		"${_BUILD_IMAGE}" \
		"${@}" ;
} ;

_main "${@}" ;
