<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text" encoding="utf-8" indent="yes"/>

	<xsl:strip-space elements="*"/>

	<xsl:variable name="multi-separator">·</xsl:variable>
	<xsl:param name="version">Unknown</xsl:param>

	<!-- Root template -->

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- Block element templates -->

	<xsl:template match="table-of-contents">
		<xsl:text>**</xsl:text>
		<xsl:text>Inhaltsverzeichnis</xsl:text>
		<xsl:value-of select="$multi-separator"/>
		<xsl:text>_Table of Contents_</xsl:text>
		<xsl:text>**    &#10;</xsl:text>
		<xsl:for-each select="/source/block/section">
			<xsl:text>[</xsl:text>
			<xsl:apply-templates/>
			<xsl:text>](#</xsl:text>
			<xsl:value-of select="replace(replace(lower-case(current()),'&amp; ',''),' ','-')"/>
			<xsl:text>)    &#10;</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="block">
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="title">
		<xsl:text># </xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="author">
		<xsl:text>## </xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="section">
		<xsl:text>## </xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="key">
		<xsl:text>**</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>**    &#10;</xsl:text>
	</xsl:template>

	<xsl:template match="value">
		<xsl:apply-templates/>
		<xsl:text>    &#10;</xsl:text>
	</xsl:template>

	<xsl:template match="quote">
		<xsl:text>>>>&#10;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;>>>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="list">
		<xsl:text>&#10;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="item">
		<xsl:text>&#10;* </xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

	<xsl:template match="footnote">
		<xsl:text>&lt;sup&gt;</xsl:text>
		<xsl:value-of select="@symbol"/>
		<xsl:text>&lt;/sup&gt;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;&#10;</xsl:text>
	</xsl:template>

	<!-- Inline element templates -->

	<xsl:template match="multi">
		<xsl:apply-templates select="*[1]"/>
		<xsl:for-each select="subsequence(*,2)">
			<xsl:value-of select="$multi-separator"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="de">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="en">
		<xsl:text>_</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>_</xsl:text>
	</xsl:template>

	<xsl:template match="strong">
		<xsl:text>◆ </xsl:text>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="normal">
		<xsl:text>◈ </xsl:text>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="weak">
		<xsl:text>◇ </xsl:text>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="article">
		<xsl:text>"</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>"</xsl:text>
	</xsl:template>

	<xsl:template match="book">
		<xsl:text>_</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>_</xsl:text>
	</xsl:template>

	<xsl:template match="link">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>](</xsl:text>
		<xsl:choose>
			<xsl:when test="@href">
				<xsl:value-of select="@href"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="text()"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template match="goto">
		<xsl:text>&lt;sup&gt;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&lt;/sup&gt;</xsl:text>
	</xsl:template>

	<xsl:template match="code">
		<xsl:text>`</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>`</xsl:text>
	</xsl:template>

</xsl:stylesheet>
