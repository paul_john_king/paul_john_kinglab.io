<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" encoding="utf-8" indent="yes"/>

	<xsl:strip-space elements="*"/>

	<xsl:variable name="multi-separator">·</xsl:variable>
	<xsl:param name="version">Unknown</xsl:param>

	<!-- Root template -->

	<xsl:template match="/">
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;&#10;</xsl:text>
		<xsl:element name="html">
			<xsl:attribute name="lang">de</xsl:attribute>
			<xsl:element name="head">
				<xsl:element name="meta">
					<xsl:attribute name="property">og:image</xsl:attribute>
					<xsl:attribute name="content">https://paul_john_king.gitlab.io/includes/images/paul_beach.jpg</xsl:attribute>
				</xsl:element>
				<xsl:element name="title"><xsl:value-of select="/source/block/title"/></xsl:element>
				<xsl:element name="link">
					<xsl:attribute name="href">includes/css/html.css</xsl:attribute>
					<xsl:attribute name="rel">stylesheet</xsl:attribute>
					<xsl:attribute name="type">text/css</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="body">
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Block element templates -->

	<xsl:template match="table-of-contents">
		<xsl:element name="div">
			<xsl:attribute name="class">key</xsl:attribute>
			<xsl:element name="span">
				<xsl:attribute name="lang">de</xsl:attribute>
				<xsl:text>Inhaltsverzeichnis</xsl:text>
			</xsl:element>
			<xsl:element name="span">
				<xsl:attribute name="class">separator</xsl:attribute>
				<xsl:value-of select="$multi-separator"/>
			</xsl:element>
			<xsl:element name="span">
				<xsl:attribute name="lang">en</xsl:attribute>
				<xsl:text>Table of Contents</xsl:text>
			</xsl:element>
		</xsl:element>
		<xsl:for-each select="/source/block/section">
			<xsl:element name="div">
				<xsl:attribute name="class">value</xsl:attribute>
				<xsl:element name="a">
					<xsl:attribute name="href">
						<xsl:text>#</xsl:text>
						<xsl:value-of select="encode-for-uri(translate(current(),' ',''))"/></xsl:attribute>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="version">
		<xsl:element name="div">
			<xsl:attribute name="class">key</xsl:attribute>
			<xsl:element name="span">
				<xsl:attribute name="lang">de</xsl:attribute>
				<xsl:text>Version</xsl:text>
			</xsl:element>
			<xsl:element name="span">
				<xsl:attribute name="class">separator</xsl:attribute>
				<xsl:value-of select="$multi-separator"/>
			</xsl:element>
			<xsl:element name="span">
				<xsl:attribute name="lang">en</xsl:attribute>
				<xsl:text>Version</xsl:text>
			</xsl:element>
		</xsl:element>
		<xsl:element name="div">
			<xsl:attribute name="class">value</xsl:attribute>
			<xsl:element name="span">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$version"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="set-div-class">
		<xsl:variable name="class">
			<xsl:value-of select="local-name()"/>
			<xsl:choose>
				<xsl:when test="@page-break='before'">
					<xsl:text> page-break-before</xsl:text>
				</xsl:when>
				<xsl:when test="@page-break='after'">
					<xsl:text> page-break-after</xsl:text>
				</xsl:when>
				<xsl:when test="@page-break='both'">
					<xsl:text> page-break-both</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="section">
		<xsl:element name="div">
			<xsl:call-template name="set-div-class"/>
			<xsl:element name="a">
				<xsl:attribute name="id">
					<xsl:value-of select="encode-for-uri(translate(current(),' ',''))"/></xsl:attribute>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="block|title|author|key|value|quote">
		<xsl:element name="div">
			<xsl:call-template name="set-div-class"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="list">
		<xsl:element name="ul">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="item">
		<xsl:element name="li">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="footnote">
		<xsl:element name="div">
			<xsl:attribute name="class">footnote</xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="@symbol"/></xsl:attribute>
			<xsl:element name="sup">
				<xsl:value-of select="@symbol"/>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- Inline element templates -->

	<xsl:template match="multi">
		<xsl:apply-templates select="*[1]"/>
		<xsl:for-each select="subsequence(*,2)">
			<xsl:element name="span">
				<xsl:attribute name="class">separator</xsl:attribute>
				<xsl:value-of select="$multi-separator"/>
			</xsl:element>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="de|en">
		<xsl:element name="span">
			<xsl:attribute name="lang"><xsl:value-of select="local-name()"/></xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="strong|normal|weak|article|book">
		<xsl:element name="span">
			<xsl:attribute name="class"><xsl:value-of select="local-name()"/></xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="link">
		<xsl:element name="a">
			<xsl:choose>
				<xsl:when test="@href">
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
					<xsl:apply-templates/>
					<xsl:element name="span">
						<xsl:attribute name="class">print-media-only</xsl:attribute>
						<xsl:text disable-output-escaping="yes"> - </xsl:text>
						<xsl:element name="code">
							<xsl:value-of select="@href"/>
						</xsl:element>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="href"><xsl:value-of select="text()"/></xsl:attribute>
					<xsl:element name="code">
						<xsl:apply-templates/>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

	<xsl:template match="goto">
		<xsl:element name="sup">
			<xsl:element name="a">
				<xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="text()" disable-output-escaping="yes"/></xsl:attribute>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Pass through templates -->

	<xsl:template match="code">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
