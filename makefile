INDEX_XML ::= index.xml
LEBENSLAUF_XML ::= Lebenslauf.xml

XSL_DIR ::= xsl
PUBLIC_DIR ::= public
INCLUDES_DIR ::= $(PUBLIC_DIR)/includes
CSS_DIR ::= $(INCLUDES_DIR)/css
FONTS_DIR ::= $(INCLUDES_DIR)/fonts

HTML_CSS ::= $(CSS_DIR)/html.css

TTF_FONTS ::= $(wildcard $(FONTS_DIR)/*/*.ttf)
EOT_FONTS ::= $(patsubst %.ttf,%.eot,$(TTF_FONTS))
AFM_FONTS ::= $(patsubst %.ttf,%.afm,$(TTF_FONTS))
WOFF_FONTS ::= $(patsubst %.ttf,%.woff,$(TTF_FONTS))
SVG_FONTS ::= $(patsubst %.ttf,%.svg,$(TTF_FONTS))

HTML_XSL ::= $(XSL_DIR)/html.xsl
MD_XSL ::= $(XSL_DIR)/md.xsl

MARGIN ::= 25mm
INDEX_HTML ::= $(PUBLIC_DIR)/index.html
LEBENSLAUF_HTML ::= $(PUBLIC_DIR)/Lebenslauf.html
LEBENSLAUF_PDF ::= $(PUBLIC_DIR)/Lebenslauf.pdf
README_MD ::= README.md

VERSION ::= $(shell\
	set -e;\
	set -u;\
	description="$$(git describe --long --dirty)";\
	timestamp="$$(git show --no-patch --format='%ci')";\
	major_minor="$${description%%-*}";\
	description="$${description\#$${major_minor}-}";\
	patch="$${description%%-*}";\
	description="$${description\#$${patch}-g}";\
	commit_id="$${description%%-*}";\
	description="$${description\#$${commit_id}}";\
	coda="$${description:+.$${description\#-}}";\
	echo "$${major_minor}.$${patch}_$${commit_id}$${coda} $${timestamp}";\
)

define create_font =
	printf "Create font '%s'.\n" "$(1)" ;\
	fontforge -lang=ff -c 'Open("$(addsuffix .ttf,$(basename $(1)))"); Generate("$(1)")' ;
endef

define lint_xml =
	printf "Lint XML file '%s'.\n" "$(1)" ;\
	xmllint --noout "$(1)" ;
endef

define lint_css =
	printf "Lint CSS file '%s'.\n" "$(1)" ;\
	output=$$(csslint --quiet "$(1)") ;\
	if test "$${output}" ;\
	then \
		echo "$${output}" ;\
		return 1 ;\
	fi ;
endef

define create_from_xml =
	printf "Create file '%s'.\n" "$(1)" ;\
	saxonb-xslt "-o:$(1)" "-s:$(word 1,$(2))" "-xsl:$(word 2,$(2))" "version=$(VERSION)" ;
endef

define create_pdf_from_html =
	printf "Create file '%s'.\n" "$(1)" ;\
	wkhtmltopdf \
		--quiet \
		--print-media-type \
		--keep-relative-links \
		--margin-top "$(MARGIN)" \
		--margin-bottom "$(MARGIN)" \
		--margin-left "$(MARGIN)" \
		--margin-right "$(MARGIN)" \
		"$(2)" "$(1)" ;
endef

define lint_html =
	printf "Lint HTML file '%s'.\n" "$(1)" ;\
	htmllint init ;\
	sed -i 's/\("id-class-style": \).*/\1false,/;s/\("indent-width": \).*/\13,/g;s/\("line-end-style": \).*/\1false,/g' .htmllintrc ;\
	output=$$(htmllint "$(1)") ;\
	rm .htmllintrc ;\
	if test $$(echo "$${output}" | wc -l) -gt 2 ;\
	then \
		echo "$${output}" ;\
		return 1 ;\
	fi ;
endef

define delete_path =
	printf "Delete path '%s'.\n" "$(1)" ;\
	rm -f -r "$(1)" ;
endef

define delete_existing_path =
	$(if $(wildcard $(1)),$(call delete_path,$(1)))
endef

.PHONY: all fonts targets source_tests target_tests clean

all: fonts targets

fonts: $(EOT_FONTS) $(WOFF_FONTS) $(SVG_FONTS)

%.eot: %.ttf
	@$(call create_font,$(@))

%.woff: %.ttf
	@$(call create_font,$(@))

%.svg: %.ttf
	@$(call create_font,$(@))

targets: source_tests $(INDEX_HTML) $(README_MD) $(LEBENSLAUF_HTML) $(LEBENSLAUF_PDF) target_tests

source_tests:
	@$(call lint_css,$(HTML_CSS))
	@$(call lint_xml,$(INDEX_XML))
	@$(call lint_xml,$(LEBENSLAUF_XML))
	@$(call lint_xml,$(HTML_XSL))
	@$(call lint_xml,$(MD_XSL))

$(INDEX_HTML): $(INDEX_XML) $(HTML_XSL)
	@$(call create_from_xml,$(@),$(^))

$(README_MD): $(LEBENSLAUF_XML) $(MD_XSL)
	@$(call create_from_xml,$(@),$(^))

$(LEBENSLAUF_HTML): $(LEBENSLAUF_XML) $(HTML_XSL)
	@$(call create_from_xml,$(@),$(^))

$(LEBENSLAUF_PDF): $(LEBENSLAUF_HTML) $(HTML_CSS)
	@$(call create_pdf_from_html,$(@),$(<))

target_tests:
	@$(call lint_html,$(INDEX_HTML))
	@$(call lint_html,$(LEBENSLAUF_HTML))

clean:
	@$(call delete_existing_path,$(INDEX_HTML))
	@$(call delete_existing_path,$(LEBENSLAUF_HTML))
	@$(call delete_existing_path,$(LEBENSLAUF_PDF))
	@$(foreach file,$(EOT_FONTS),$(call delete_existing_path,$(file)))
	@$(foreach file,$(WOFF_FONTS),$(call delete_existing_path,$(file)))
	@$(foreach file,$(SVG_FONTS),$(call delete_existing_path,$(file)))
	@$(foreach file,$(AFM_FONTS),$(call delete_existing_path,$(file)))
